from rest_framework.throttling import AnonRateThrottle


class CreateUserRateThrottle(AnonRateThrottle):
    rate = "100/hour"
