from rest_framework import serializers as sl
from django.contrib.auth import get_user_model

User = get_user_model()


class UserSerializer(sl.ModelSerializer):
    class Meta:
        model = User
        fields = (
            "id",
            "username",
            "is_active",
            "is_staff",
            "is_superuser",
            "date_joined",
        )
        read_only_fields = (
            "username",
            "is_active",
            "is_staff",
            "is_superuser",
            "date_joined",
        )

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data["password"])
        user.save()
        return user
