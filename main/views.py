from django.contrib.auth import get_user_model
from rest_framework import generics
from rest_framework import filters
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly

from main.serializers import UserSerializer
from main.throttles import CreateUserRateThrottle
from main.permissions import IsAdminExceptForCreate, IsOwnerOrAdmin


User = get_user_model()


@api_view(["GET"])
def api_root(request, format=None):
    return Response({"users": reverse("user-list", request=request, format=format),})


class UserList(generics.ListCreateAPIView):
    throttle_classes = (CreateUserRateThrottle,)
    permission_classes = (IsAuthenticatedOrReadOnly,)
    filter_backends = (filters.SearchFilter,)
    search_fields = ("username", "email")
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filterset_fields = ["username", "email"]


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = User.objects.all()
    serializer_class = UserSerializer
