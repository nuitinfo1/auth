from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from main import views


urlpatterns = [
    path("", views.api_root, name="root-v1"),
    path("users", views.UserList.as_view(), name="user-list"),
    path("users/<str:pk>", views.UserDetail.as_view(), name="user-detail"),
]

urlpatterns = format_suffix_patterns(urlpatterns)
