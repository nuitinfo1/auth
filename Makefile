.PHONY: serve test migrate

serve:
	pipenv run python manage.py runserver

test:
	pipenv run python manage.py test

migrate:
	pipenv run python manage.py migrate
