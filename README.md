# Service d'authentification

Ce service fourni la partie utilisateur ainsi que l'authentification (grace à JWT) pour les autres services.

Ce projet utilise python3.7, pipenv et django-rest-framework.

## Environnement de dévellopement

```sh
python3.7 -m pip install -U pipenv
pipenv install --dev
pipenv run python manage.py migrate
pipenv run python runserver 
```
