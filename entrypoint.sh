#!/bin/bash
pipenv install
pipenv run python manage.py migrate
pipenv run python manage.py collectstatic --noinput
pipenv run gunicorn -w 2 -b 0.0.0.0:8003 auth.wsgi